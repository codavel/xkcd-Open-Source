### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an Android app, by modifying xkcd-Open-Source, , a free, ad-free, open-source, native, and universal xkcd.com reader for iOS.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/mamaral/xkcd-Open-Source).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/apps-android-wikipedia/-/commit/9872d4352a52b0d22614e3a1017d6e5740f08a3b)):

1. Include Codavel's dependency into the applications Podfile, required to download and use Codavel's SDK
2. Start Codavel's Service with Codavel's Application ID and Secret when the app starts, by changing the didFinishLaunchingWithOptions method in the AppDelegate.
3. Register our HTTP interceptor into the app's default session configuration and SDWebImageDownloader's session configuration, so that all the HTTP requests executed through the app's are processed and forwarded to our SDK.
